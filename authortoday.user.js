// ==UserScript==
// @name         Download AuthorToday
// @match        https://author.today/reader/*
// @icon         https://www.google.com/s2/favicons?domain=author.today
// @grant        none
// ==/UserScript==

function createButton(label, onClick) {
    var btn = document.createElement("BUTTON");
    btn.innerHTML = label;
    btn.onclick = onClick;
    document.querySelector(".reader-content.hidden-print").prepend(btn);
}

function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

function check(changes, observer) {
    if(document.querySelector("#text-container").innerHTML.length > 50) {
        observer.disconnect();
        createButton("Download",() => {
            download(prompt("Enter file number")+".html",document.querySelector("#text-container").innerHTML);
        });
    }
}

(function() {
    (new MutationObserver(check)).observe(document, {childList: true, subtree: true});
})();
